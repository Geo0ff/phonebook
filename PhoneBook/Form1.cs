﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Recognition;
using System.Speech.Synthesis;

namespace PhoneBook
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            try
            {


                panel1.Enabled = true;
                App.PhoneBook.AddPhoneBookRow(App.PhoneBook.NewPhoneBookRow());
                phoneBookBindingSource.MoveLast();
                txtPhoneNumber.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                App.PhoneBook.RejectChanges();
            }

        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            txtPhoneNumber.Focus();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            phoneBookBindingSource.ResetBindings(false);
            panel1.Enabled = false;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                phoneBookBindingSource.EndEdit();
                App.PhoneBook.AcceptChanges();
                App.PhoneBook.WriteXml(string.Format("{0}//data.dat", Application.StartupPath));
                panel1.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                App.PhoneBook.RejectChanges();
            }
        }
        static AppData db;
        protected static AppData App
        {
            get
            {
                if (db == null)
                    db = new AppData();
                return db;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string fileName = string.Format("{0}//data.dat", Application.StartupPath);
            if(File.Exists(fileName))
                App.PhoneBook.ReadXml(fileName);
            phoneBookBindingSource.DataSource = App.PhoneBook;
            panel1.Enabled = false;
        }

        private void DataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (MessageBox.Show("Are you sure want to delete this record?","Message",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
                    phoneBookBindingSource.RemoveCurrent();

            }
                

        }

        private void TextSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (!string.IsNullOrEmpty(textSearch.Text))
                {
                    var query = from o in App.PhoneBook
                                where o.PhoneNumber==textSearch.Text||o.FullName.Contains(textSearch.Text)||o.Email==textSearch.Text
                                select o;
                    dataGridView.DataSource = query.ToList();
                }
                else
                    dataGridView.DataSource = phoneBookBindingSource;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SpeechSynthesizer sSynth = new SpeechSynthesizer();
            PromptBuilder pBuilder = new PromptBuilder();
            SpeechRecognitionEngine sRecogniize = new SpeechRecognitionEngine();



            Choices sList = new Choices();
            sList.Add(new string[] { "hello" });
            Grammar gr = new Grammar(new GrammarBuilder(sList));

            try
            {
                sRecogniize.RequestRecognizerUpdate();
                sRecogniize.LoadGrammar(gr);
                sRecogniize.SpeechRecognized += sRecognize_SpeechRecognized;
                sRecogniize.SetInputToDefaultAudioDevice();
                sRecogniize.RecognizeAsync(RecognizeMode.Multiple);
                sRecogniize.Recognize();

            }
            catch
            {
                return;
            }
        }
        private void sRecognize_SpeechRecognized(Object sender, SpeechRecognizedEventArgs e)
        {
            if (e.Result.Text == "test") 
            {
                MessageBox.Show("Hello");
                label7.Text = "it works";
            }
        }
    }
}

